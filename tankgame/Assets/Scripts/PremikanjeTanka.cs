﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PremikanjeTanka : MonoBehaviour
{
    // Start is called before the first frame update
    public float movespeed = 5f;
    public Rigidbody2D rb;
    Vector2 movement;
    public GameObject[] spawnPoints;
    private void Awake()
    {

    }
    private void Start()
    {
        spawnPoints = GameObject.FindGameObjectsWithTag("Respawn");
        Spawn();
    }

    private void Spawn()
    {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        transform.position = spawnPoints[spawnPointIndex].transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        
        Vector3 pos = transform.position;

        if (Input.GetKey("w"))
        {
            Vector3 posChange = new Vector3(movespeed * Time.deltaTime,0 , 0);
            pos -= transform.rotation * posChange;
        }
        if (Input.GetKey("s"))
        {
            Vector3 posChange = new Vector3(movespeed * Time.deltaTime,0, 0);
            pos += transform.rotation * posChange;
        }
        if (Input.GetKey("d"))
        {
            transform.Rotate(Vector3.back);//obracanje v smeri urinega kazalca
            //pos.x += movespeed * Time.deltaTime;
        }
        if (Input.GetKey("a"))
        {
            transform.Rotate(Vector3.forward);//obracanje v nasprotni smeri
            //pos.x -= movespeed * Time.deltaTime;
        }
        if (Input.GetKey("r"))
        {

            pos = GameObject.FindWithTag("Respawn").transform.position;
        }


        transform.position = pos;
    }
    //void FixedUpdate()
    //{
    //    rb.MovePosition(rb.position + movement * movespeed * Time.fixedDeltaTime);
    //}
}
